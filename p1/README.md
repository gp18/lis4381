## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Project #1 Requirements:

*Five Parts:*

1. Develop an application that:
   1. Shows a launcher icon image and displays it both activities (screens).
   2. Adds background color(s) to both activities.
   3. Adds border around image and button.
   4. Changes the text shadow.
2. Skill set 7: Random Array Data Validation
3. Skill set 8: Largest of Three Numbers
4. Skill set 9: Array Runtime Data Validation
5. Chapter questions (Chs. 7, 8)

#### README.md file should include the following items:

- Screenshots of business card home page running

- Screenshot of business card details page running

- Screenshots of Random Array Data Validation application running

- Screenshot of Largest of Three Numbers application running

- Screenshot of Array Runtime Data Validation application running

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  

  *Screenshot of Business Card*:
  
  | First User Interface                       | Second User Interface                      |
  | ------------------------------------------ | ------------------------------------------ |
  | ![Business Card 1](img/businesscard_1.png) | ![Business Card 2](img/businesscard_2.png) |
  
  
  
  *Screenshot of java skillsets:*
  
  | Skill Set 7: Random Array Data Validation               | Skill Set 8: Largest of Three Numbers          | Skill Set 9: Array Runtime Data Validation               |
  | ------------------------------------------------------- | ---------------------------------------------- | -------------------------------------------------------- |
  | ![SS7 Screenshot](img/random_array_data_validation.png) | ![SS8 Screenshot](img/largestthreenumbers.png) | ![SS9 Screenshot](img/array_runtime_data_validation.png) |
