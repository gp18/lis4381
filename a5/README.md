## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Assignment #5 Requirements:

*Three Parts:*

2. Local portfolio requirements
3. Chapter questions (Chs. 11, 12, 19)

#### README.md file should include the following items:

- Screenshots of local LIS4381 web app portfolio

- Screenshots of Java skill sets

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  *Screenshot of LIS4381 Portal (Main Page):*

  ![main page Screenshot](img/a3_erd.png)
  
  *Screenshot of Failed Validation*:

  
  
  *Screenshot of Passed Validation:*
  
  
  
  *Screenshot of java skillsets:*
  
  | Skill Set 13: Sphere Volume Calculator         | Skill Set 14: PHP Simple Calculator               | Skill Set 15: PHP Write/Read File                  |
  | ---------------------------------------------- | ------------------------------------------------- | -------------------------------------------------- |
  | ![SS13 Screenshot](img/sphere_volume_calc.png) | ![SS14 Screenshot](img/alpha_numeric_special.png) | ![SS15 Screenshot](img/temperature_conversion.png) |
  

Links:

​	a. Link to local LIS4381 web app: http://localhost/repos/lis4381/

