## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Assignment #2 Requirements:

*Three Parts:*

1. Healthy Recipe Mobile App
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link

#### README.md file should include the following items:

- Screenshots of running mobile app

- Screenshot of Java skill sets

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  *Screenshots of Healthy Recipes Mobile App:*

  | Splash Screen                                        | Ingredients Screen                                  |
  | ---------------------------------------------------- | --------------------------------------------------- |
  | ![Healthy Reciples Screenshot](img/bruschetta_1.png) | ![Healthy Recipes Screenshot](img/bruschetta_2.png) |

  *Screenshots of Java Skill Sets:*

  | Skill Set 1: Even or Odd                           | Skill Set 2: Largest of Two Integers                  | Skill Set 3: Arrays and Loops                            |
  | -------------------------------------------------- | ----------------------------------------------------- | -------------------------------------------------------- |
  | ![SS1 Screenshot](img/_EvenOrOdd_Java_Program.png) | ![SS2 Screenshot](img/LargestNumber_Java_Program.png) | ![Arrays and Loops Screenshot](img/Arrays_And_Loops.png) |

