> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Gabin Park

### LIS4381 Requirements:

*Course Work Links:* 

[A1 README.md](a1/README.md "my a1 readme file")

- ​    Install AMPPS

- ​    Install JDK

- ​    Install Android Studio and create My First App

- ​    Provide screenshots of installations

- ​    Create Bitbucket repo

- ​    Complete Bitbucket tutorials (bitbucketstationlocations)

- ​    Provide git command descriptions

[A2 README.md](a2/README.md "my a2 readme file")

- ​    Create Healthy Recipes Android app

- ​    Provide screenshots of completed app

[A3 README.md](a3/README.md "my a3 readme file")

- Create ERD based upon business rules
- Provide screenshot of completed ERD
- Provide DB resource links

[A4 README.md](a4/README.md "my a4 readme file")

- Create local portfolio
- Provide screenshots of skillsets

[A5 README.md](a5/README.md "my a5 readme file")

- Create local porfolio
- Provide screenshots of skillsets

[P1 README.md](p1/README.md "my p1 readme file")

- Create an application that can be used as a business card (includes contact info and interests)
- Provide screenshots of completed app
- Provide screenshots of skill sets

[P2 README.md](p2/README.md "my p2 readme file")

- Create local porfolio
