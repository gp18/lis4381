## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Assignment #4 Requirements:

*Three Parts:*

1. Favicon Requirements
2. Local portfolio requirements
3. Chapter questions (Chs. 9, 10, 15)

#### README.md file should include the following items:

- Screenshots of local LIS4381 web app portfolio

- Screenshots of Java skill sets

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  *Screenshot of LIS4381 Portal (Main Page):*

  ![main page Screenshot](img/a3_erd.png)
  
  *Screenshot of Failed Validation*:

  
  
  *Screenshot of Passed Validation:*
  
  
  
  *Screenshot of java skillsets:*
  
  | Skill Set 10: Array List             | Skill Set 11: Alpha Numeric Special               | Skill Set 12: Temperature Conversion               |
  | ------------------------------------ | ------------------------------------------------- | -------------------------------------------------- |
  | ![SS4 Screenshot](img/arraylist.png) | ![SS11 Screenshot](img/alpha_numeric_special.png) | ![SS12 Screenshot](img/temperature_conversion.png) |
  

Links:

​	a. Link to local LIS4381 web app: http://localhost/repos/lis4381/

