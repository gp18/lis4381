## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Project 2 Requirements:

*Two Parts:*

2. Local portfolio requirements
3. Chapter questions (Chs. 13, 14)

#### README.md file should include the following items:

- Screenshots of local LIS4381 web app portfolio

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  *Screenshot of LIS4381 Carousel:*

  ![main page Screenshot](img/carousel.png)

  *Screenshot of index.php*:

  ![index Screenshot](img/index.png)

  *Screenshot of edit_petstore.php:*

  ![edit_petstore Screenshot](img/edit_petstore.png)

  *Screenshot of Failed Validation*

  ![failed_validation Screenshot](img/failed_validation.png)

  *Screenshot of Passed Validation*

  ![passed_validation Screenshot](img/passed_validation.png)

  *Screenshot of Delete Record Prompt*

  ![delete_record_prompt Screenshot](img/delete_record_prompt.png)

  *Screenshot of Deleted Record*

  ![deleted_record Screenshot](img/deleted_record.png)

  *Screenshot of RSS feed*

  ![rss_feed Screenshot](img/rss_feed.png)


Links:

​	a. Link to local LIS4381 web app: http://localhost/repos/lis4381/

