import java.util.Scanner;

public class Main
{
    public static void main(String args[])
    {
        System.out.println("\nDeveloper: Gabin Park");
        System.out.println("Program evaluates user-entered characters.\n");
        System.out.println("phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");

        Methods.decisionStructures();
    }
}