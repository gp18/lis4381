import java.util.Scanner;

class Main
{
    public static void main(String args[])
    {
        Methods.getRequirements();

        System.out.println("***Call static (no object) void (non-value returning) method:***");
        //call static void method (i.e., no object, non-value returning)
        Methods.largestNumber();

        //or..
        System.out.println("\n***Call static (no object) value-returning method and void method:***");
        //declare variables and create Scanner object
        int myNum1=0, myNum2=0;

        //call static value-returning method(i.e., no object)
        System.out.print("Enter first integer: ");
        myNum1 = Methods.getNum();

        System.out.print("Enter second integer: ");
        myNum2 = Methods.getNum();

        //call void method passing user input
        Methods.evaluateNumber(myNum1, myNum2);
    }
}