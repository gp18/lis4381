import java.util.Scanner;

public class Methods

{


	public static void getRequirements()

	{

		//display operational messages
		
		System.out.println("Developer: Gabin Park");
		System.out.println("Program loops through array of strings.");
		System.out.println("Use following values: dog, cat, bird, fish, insect.");
		System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
		
		System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do... while.");
		System.out.println(); //print blank line

	}
	//nonvalue-returning method (without object - static)
	public static void arrayLoops()
	{

		/*
			Java example:
			//declare String array, with initial size of 5
			String[] animals = new String[5];

			//populate array (zero-based)
			animals[0] = "dog";
			animals[1] = "cat";
			animals[3] = "bird";
			animals[4] = "fish";
			animals[5] = "insect";
		*/

		String animals[] = {"dog", "cat", "bird", "fish", "insect"};

		System.out.println("for loop: ");
		for(int i=0; i < animals.length; i++)
		{
			System.out.println(animals[i]);
		}

		System.out.println("\nEnhanced for loop:");
		for(String test : animals)
		{
			System.out.println(test);
		}
		System.out.println("\nwhile loop: ");
		int i=0;
		while (i < animals.length)
		{
			System.out.println(animals[i]);
			i++;
		}
		i=0; //reassign 0 to test variable
		System.out.println("\ndo...while loop.");
		do
		{
			System.out.println(animals[i]);
			i++;
		}
		while(i < animals.length);
	}
}