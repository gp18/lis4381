## LIS 4381 - Mobile Web Application Development

## Gabin Park

### Assignment #3 Requirements:

*Three Parts:*

1. Database Requirements
2. Mobile App requirements
3. Chapter questions (Chs. 5, 6)

#### README.md file should include the following items:

- Screenshots of running mobile app

- Screenshot of ERD

- Screenshots of Java skill sets

  

  > This is a blockquote.
  >
  > This is the second paragraph in the blockquote.

  ### Assignment Screenshots:

  *Screenshot of ERD:*

  ![A3 ERD Screenshot](img/a3_erd.png)
  
  *Screenshot of Concert Tickets App*:

  | First User Interface                                      | Second User Interface                          |
  | --------------------------------------------------------- | ---------------------------------------------- |
  | ![Concert Ticket Screenshot 1](img/concert_tickets_1.png) | ![Concert Ticket 2](img/concert_tickets_2.png) |
  
  *Screenshot of 10 records for each table:*
  
  | Petstore:                              | Customer:                              | Pet:                         |
  | -------------------------------------- | -------------------------------------- | ---------------------------- |
  | ![Petstore Table](img/a3_petstore.png) | ![Customer Table](img/a3_customer.png) | ![Pet Table](img/a3_pet.png) |
  
  *Screenshot of java skillsets:*
  
  | Skill Set 4: Decision Structures               | Skill Set 5: Nested Structures               | Skill Set 6: Methods               |
  | ---------------------------------------------- | -------------------------------------------- | ---------------------------------- |
  | ![SS4 Screenshot](img/Decision_Structures.png) | ![SS5 Screenshot](img/Nested_Structures.png) | ![SS6 Screenshot](img/methods.png) |
  

Links:

​	a. a3.mwb: [a3.mwb](https://bitbucket.org/gp18/lis4381/src/master/a3/docs/a3.mwb)

​	b. a3.sql: [a3.sql](https://bitbucket.org/gp18/lis4381/src/master/a3/docs/a3.sql)

